package misyac.yuri.home_work_4;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private static final String STATE_BITMAP = MainActivity.class.getCanonicalName() + "#bitmap";

    private ImageView mImageView;
    private Button mInvert;
    private Button mCrop;
    private Button mGradient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mImageView = (ImageView) findViewById(R.id.imageView);
        mInvert = (Button) findViewById(R.id.invert);
        mCrop = (Button) findViewById(R.id.crop);
        mGradient = (Button) findViewById(R.id.gradient);

        if (savedInstanceState != null) {
            mImageView.setImageBitmap((Bitmap) savedInstanceState.getParcelable(STATE_BITMAP));
        }

        mInvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                invertRBChannels();
            }
        });
        mCrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                crop();
            }
        });
        mGradient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gradient();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh:
                mImageView.setImageResource(R.drawable.android_pay);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(STATE_BITMAP, getImageViewBitmap());
    }

    private void gradient() {
        Bitmap original = getImageViewBitmap();

        Bitmap bitmap = Bitmap.createBitmap(
                original.getWidth(),
                original.getHeight(),
                Bitmap.Config.ARGB_8888
        );


        Canvas canvas = new Canvas(bitmap);
        canvas.drawBitmap(original, 0, 0, null);

        Shader shader = new RadialGradient(200, 200, 200, getColors(), null, Shader.TileMode.CLAMP);
        Paint paint = new Paint();
        paint.setDither(true);
        paint.setShader(shader);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DARKEN));
        canvas.drawCircle(200, 200, 200, paint);

        mImageView.setImageBitmap(bitmap);
    }

    private int[] getColors() {
        return new int[]{
            ContextCompat.getColor(this, android.R.color.holo_red_light),
            ContextCompat.getColor(this, android.R.color.holo_green_light),
            ContextCompat.getColor(this, android.R.color.holo_blue_light)
        };
    }

    private void crop() {
        Bitmap original = getImageViewBitmap();

        Bitmap bitmap = Bitmap.createBitmap(
                original.getWidth(),
                original.getHeight(),
                Bitmap.Config.ARGB_8888
        );

        Canvas canvas = new Canvas(bitmap);
        canvas.drawBitmap(original, 0, 0, null);

        Paint maskPaint = new Paint();
        maskPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        canvas.drawBitmap(triangleMask(), 0, 0, maskPaint);

        mImageView.setImageBitmap(bitmap);
    }

    private Bitmap triangleMask() {
        Bitmap original = getImageViewBitmap();

        Bitmap bitmap = Bitmap.createBitmap(
                original.getWidth(),
                original.getHeight(),
                Bitmap.Config.ARGB_8888
        );

        Canvas canvas = new Canvas(bitmap);

        Paint paint = new Paint();
        paint.setColor(Color.TRANSPARENT);
        canvas.drawPaint(paint);

        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.GREEN);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setAntiAlias(true);

        int size = getResources().getDimensionPixelSize(R.dimen.triangle_size);
        int ml = getResources().getDimensionPixelSize(R.dimen.triangle_ml);
        int mt = getResources().getDimensionPixelSize(R.dimen.triangle_mt);

        Point a = new Point(ml + size/2, mt);
        Point b = new Point(ml, size + mt);
        Point c = new Point(ml + size, size + mt);

        Path path = new Path();
        path.setFillType(Path.FillType.EVEN_ODD);
        path.moveTo(a.x, a.y);
        path.lineTo(b.x, b.y);
        path.lineTo(c.x, c.y);
        path.lineTo(a.x, a.y);
        path.close();

        canvas.drawPath(path, paint);

        return bitmap;
    }

    private void invertRBChannels() {
        Bitmap original = getImageViewBitmap();

        Bitmap bitmap = Bitmap.createBitmap(
                original.getWidth(),
                original.getHeight(),
                Bitmap.Config.ARGB_8888
        );

        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        paint.setColorFilter(new ColorMatrixColorFilter(getColorMatrix()));
        canvas.drawBitmap(original, 0, 0, paint);

        mImageView.setImageBitmap(bitmap);
    }

    private Bitmap getImageViewBitmap() {
        BitmapDrawable drawable = (BitmapDrawable) mImageView.getDrawable();
        return drawable.getBitmap();
    }

    private ColorMatrix getColorMatrix() {
        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(0);

        ColorMatrix scaleMatrix = new ColorMatrix(new float[]{
                0, 0, 0, 1, 0,
                0, -1, 0, 1, 0,
                0, 0, -1, 1, 0,
                0, 0, 0, 1, 0,
        });

        colorMatrix.postConcat(scaleMatrix);

        return colorMatrix;
    }
}
